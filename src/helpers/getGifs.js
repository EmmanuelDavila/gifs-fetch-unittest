export const getGifs = async (category) => {

    const url = `https://api.giphy.com/v1/gifs/search?q=${encodeURI(category)}&limit=15&api_key=lDiqi3bgqzdWIOCd4VyVu3pFaartHA8K`
    const respuesta = await fetch(url);
    const { data } = await respuesta.json();
    const gifs = data.map(img => {
        return {
            id: img.id,
            url: img.images?.downsized.url,
            title: img.title,
        }
    })
    console.log('data', gifs);
    return gifs;
}