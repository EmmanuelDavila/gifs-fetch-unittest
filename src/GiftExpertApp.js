import React, { useState } from 'react'
import { AddCategory } from './components/AddCategory'
import { GifGrid } from './components/GifGrid'

export const GiftExpertApp = ({defaultCategories = []}) => {

    // const categories = ['Zelda', 'samurai X', 'Wow']

    // const [categories, setCategories] = useState(['Zelda']);
    const [categories, setCategories] = useState(defaultCategories)


    // const handleAdd = () =>{
    //   //  setCategories([...categories, 'Metallica']);
    //   setCategories(res => [...res, 'HunterXHunter'])
    // }

    return (
        <>
        <h2>GiftExpertApp</h2>
        <AddCategory setCategories={setCategories}/>
        <hr/>
     
        <ol>
            { categories.map((datos) =>(
                <GifGrid key={datos}
                 category={datos}/>
            )) 
            }
        </ol>
        </>
    )
}
