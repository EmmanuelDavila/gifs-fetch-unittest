import React from 'react'
import PropTypes from 'prop-types';
export const GiftGridItem = ({ title, url }) => {

    return (
        <div className="items animate__animated animate__fadeIn">
            <img src={url} alt={title} />
            <h3>{title}</h3>
        </div>
    )
}


GiftGridItem.propTypes = {
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired
}