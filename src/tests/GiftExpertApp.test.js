import { shallow } from 'enzyme'
import '@testing-library/jest-dom'
import React from 'react';
import { GiftExpertApp } from '../GiftExpertApp';


describe('Pruebas en GiftExpertApp', () => {

    const wrapper = shallow(<GiftExpertApp />)

    test('Debe mostrarse correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    });

    test('Debe de mostrar una lista de categorias', () => {
        const categorias = ['Dragon ball', 'Zelda'];

        const wrapper = shallow(<GiftExpertApp defaultCategories={categorias} />);

        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find('GifGrid').length).toBe(categorias.length);
    });


})
