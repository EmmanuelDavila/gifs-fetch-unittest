import React from 'react';
import { shallow } from 'enzyme'
import { GiftGridItem } from '../components/GiftGridItem';
import '@testing-library/jest-dom'


describe('Pruebas en GiftApp', () => {

    const urlTest = 'https://api.giphy.com/v1/gifs/search?q=zelda&limit=35&api_key=lDiqi3bgqzdWIOCd4VyVu3pFaartHA8K'
    const title = 'Zelda'
    let wrapper = shallow(<GiftGridItem title={title} url={urlTest} />);

    beforeEach(() => {
        shallow(<GiftGridItem />);
    });

    test('should mostrar el componente correctamener', () => {
        expect(wrapper).toMatchSnapshot();
    });

    test('Debe de tener un h3 con el titulo', () => {
        const h3 = wrapper.find('h3');
        expect(h3.text().trim()).toBe(title)
    });

    test('Debe de tener la imagen igual a url y alt de los props', () => {
        const img = wrapper.find('img');
        // console.log(img.props());
        expect(img.prop('src')).toBe(urlTest);
        expect(img.prop('alt')).toBe(title);
    });

    test('Debe tener animate__fadeIn', () => {
        const div = wrapper.find('div');
        // console.log(div.props().className);
        //OPTION1
        // expect(div.prop('className')).toBe('items animate__animated animate__fadeIn');
        //OPTION 2
        const className = div.props().className;
        expect(className.includes('items animate__animated animate__fadeIn')).toBe(true);
    });
})
