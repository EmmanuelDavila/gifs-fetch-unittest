import React from 'react';
import { shallow } from "enzyme";
import { AddCategory } from '../components/AddCategory';
import '@testing-library/jest-dom';

describe('Pruebas en el component <AddCategory/>', () => {

    const setCategories = jest.fn();
    let wrapper = shallow(<AddCategory setCategories={setCategories} />);

    beforeEach(() => {
        jest.clearAllMocks();
        wrapper = shallow(<AddCategory setCategories={setCategories} />);
    });

    test('Dede de mostrarse correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    });

    test('Debe de cambiar la caja de texto', () => {
        const input = wrapper.find('input');
        const value = 'Chadwick'
        input.simulate('change', {
            target: {
                value
            }
        });
    });

    test('NO debe de postear la información con submit', () => {
        wrapper.find('form').simulate('submit', { preventDefault() { } });
        expect(setCategories).not.toHaveBeenCalled();
    });

    test('Debe de ñña,ar eñ setCategopries y limpiar lña caja de texto', () => {
        const value = 'Chadwick1'
        wrapper.find('input').simulate('change', {
            target: {
                value
            }
        })
        wrapper.find('form').simulate('submit', { preventDefault() { } })
        expect(setCategories).toHaveBeenCalled();
        expect(setCategories).toHaveBeenCalledTimes(1);
        expect(setCategories).toHaveBeenCalledWith(expect.any(Function));
        expect(wrapper.find('input').prop('value')).toBe('');
    });
});
