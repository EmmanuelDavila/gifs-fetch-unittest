import '@testing-library/jest-dom'
import { useFetchGifs } from '../hooks/useFetchGifs';
import { renderHook } from '@testing-library/react-hooks';

describe('Pruebas en useFetch', () => {

    test('Debe de retornar el estado inicial', async () => {

        //  const { data, loading } = useFetchGifs('zelda');
        //  Custom hooks TEST

        const { result, waitForNextUpdate } = renderHook(() => useFetchGifs('zelda'));
        const { data, loading } = result.current;
        await waitForNextUpdate();
        expect(data).toEqual([]);
        expect(loading).toBeTruthy();
    });

    test('Debe de retornar un arreglo de imgs y el loading en false', async () => {

        const { result, waitForNextUpdate } = renderHook(() => useFetchGifs('zelda'));

        await waitForNextUpdate();

        const { data, loading } = result.current;
        expect(data.length).toBe(15);
        expect(loading).toBe(false);
    });


});
