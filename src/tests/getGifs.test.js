import { shallow } from 'enzyme'
import '@testing-library/jest-dom'
import { getGifs } from '../helpers/getGifs'


describe('Pruebas en el helpers Gifs Fetch', () => {

    test('Debe de traer 15 elementos', async () => {
        const gifs = await getGifs('Zelda')
        expect(gifs.length).toBe(15)
    });

    test('Debe tener 0 items si no se le pasa una cartegoria', async () => {
        const gifs = await getGifs('');
        expect(gifs.length).toBe(0)
    });

})
